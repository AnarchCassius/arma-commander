#include "\AC\defines\commonDefines.inc"

ACF_ai_strategyAgent = {
	while {!AC_ended} do {
		// Prepare data for strategy decisions
		{
			[_x] call ACF_ai_calculateDefensesAndThreat;
			true
		} count AC_bases;
		sleep 1;

		//Run AI planning
		{
			if (IS_AI_ENABLED(_x)) then {
				[_x] call ACF_ai_assignAttacks;
				sleep 1;
				[_x] call ACF_ai_assignDefenders;
				sleep 1;
				[_x] call ACF_assignIdleGroups;
			};
			true
		} count AC_battalions;
		sleep STRATEGY_TICK-1;
	};
};

// Find out current defenses for the base - both their number and list of all units
ACF_ai_calculateDefensesAndThreat = {
	params ["_base"];
	private _side = GVAR(_base,"side");
	if (_side == sideEmpty) exitWith {
		_base setVariable ["def_currentStr",0]; // Strength of units assigned to defense of the base
		_base setVariable ["thr_currentStr",0];
		_base setVariable ["att_costDet",0]; // How "expensive" is detected to attack into a location at the moment
		_base setVariable ["def_costDet",0]; // How "expensive" is detected to defend a location at the moment
		_base setVariable ["thr_currentDetStr",0];
		_base setVariable ["def_currentDetStr",0]; // How many detected units are assigned to defense of the base

	};
   	private _enemySide = [_side] call ACF_enemySide;
	private _friendlyGroups = (AC_operationGroups select {side _x == _side });
	private _enemyGroups = AC_operationGroups - _friendlyGroups;
	private _defCurrentStr = 0;
	private _defCurrentStrDet = 0;

	//determine defender detection type
	private _color = switch (_side) do {
	    case west: { "Wdetected" };
	    case east: { "Edetected" };
	    case resistance: { "Idetected" };
	    default { "Wdetected"};
	};

	//determine enemy detection type
	private _color2 = switch (_enemySide) do {
	    case west: { "Wdetected" };
	    case east: { "Edetected" };
	    case resistance: { "Idetected" };
	    default { "Edetected"};
	};
	private _defCurrent = [];
	private _thrCurrent = [];
	/*
	if GVARS(_base,"deployed",DEPLOYED_TRUE) then {
		_defCurrent = _defCurrent + [GVAR(_base,"staticGroup")]
	};
	*/
	{
		_defCurrent pushbackUnique _x;
		true
	} count (_friendlyGroups select {leader _x distance _base < DEFENSE_DISTANCE});

	/*
	private _defAssigned = _defCurrent;
	{
		_defAssigned pushbackunique _x;
		true
	} count (GVARS(_base,"defenders",[]));
	*/

	{
		_defCurrentStr = _defCurrentStr + ([_x] call ACF_ai_groupStrength);
		if (GVARS(_x,_color2,false)) then {
			_defCurrentStrDet = _defCurrentStrDet + ([_x] call ACF_ai_groupStrength);
		};
		true
	} count _defCurrent;

	{
		_defCurrentStr = _defCurrentStr + PLAYER_STRENGTH;
		true
	} count (allPlayers select {private _p = _x; _defCurrent findIf {group _p == _x} > -1});

	private _thrCurrentStr = 0;
	private _thrCurrentStrDet = 0;
	private _baseThreatDistance = THREAT_DISTANCE + GVAR(_base,"out_perimeter");
	_thrCurrent append _enemyGroups select {_base distance leader _x < _baseThreatDistance};
	/*
	private _thrAssigned = _thrCurrent;
	{
		_thrAssigned pushbackunique _x;
		true
	} count (_enemyGroups select {getWPPos [_x, currentWaypoint _x] distance _base < _baseThreatDistance});
	*/

	{
		_thrCurrentStr = _thrCurrentStr + ([_x] call ACF_ai_groupStrength);
		if (GVARS(_x,_color,false)) then {
			_thrCurrentStrDet = _thrCurrentStrDet + ([_x] call ACF_ai_groupStrength);
		};
		true
	} count _thrCurrent;

	{
		_thrCurrentStr = _thrCurrentStr + PLAYER_STRENGTH;
		true
	} count (allPlayers select {private _p = _x; _thrCurrent findIf {group _p == _x} > -1});

	// What exacly is defenseCost?
	private _attackCostDet = _defCurrentStrDet * ATTACK_DEFENSE_RATIO;
	private _defenseCostDet = _thrCurrentStrDet * DEFENSIVE_RATIO - _defCurrentStr;

	//_base setVariable ["def_current",_defCurrent]; // What units are actually defending
	_base setVariable ["def_currentStr",_defCurrentStr]; // Strength of units assigned to defense of the base
	//_base setVariable ["def_total",_defAssigned]; // What units are near base and what units are assigned
	//_base setVariable ["thr_current",_thrCurrent]; // How many enemy units are around the base
	_base setVariable ["thr_currentStr",_thrCurrentStr];
	//_base setVariable ["thr_total",_thrAssigned]; // Enemy units around the base and assigned to attack
	_base setVariable ["att_costDet",_attackCostDet]; // How "expensive" is detected to attack into a location at the moment
	_base setVariable ["def_costDet",_defenseCostDet]; // How "expensive" is detected to defend a location at the moment
	_base setVariable ["thr_currentDetStr",_thrCurrentStrDet];
	_base setVariable ["def_currentDetStr",_defCurrentStrDet]; // How many detected units are assigned to defense of the base

	//if(DEBUG_MODE) then {systemChat format ["%1 threat",_thrCurrentStrDet]};
	//if(DEBUG_MODE) then {systemChat format ["%1 def",_defCurrentStrDet]};
};

// Debug hint of all base statuses
ACF_ai_showAiInfo = {
	private _hint = "Name: thrtC / thrtT / defC / defT / DEF \n";
	{
		private _str = format ["%1: AC: %2 /DC: %3 / %4\n",
			_x getVariable "callsign",
			GVAR(_x,"att_costDet"),
			GVAR(_x,"def_costDet"),
			GVARS(_x,"defended",false)
		];
		_hint = _hint + _str;
		true
	} count AC_bases;

	// List of attacks
	_hint = _hint + "\n Attacks: \n";

	hintSilent format ["%1",_hint];
};


ACF_ai_groupStrength = {
	params ["_group"];
 	private _groupUnits = units _group;
	private _result = GVARS(_group, "str", 0);
	if (_result > 0 && GVARS(_group, "resetStr", false) == false) exitWith {_result};
	if (!isNull GVARS(_group,"base",objNull)) then {
		_result = 1;
	};

	{
		_result = _result + ((_x call BIS_fnc_threat)*2) + 0.5;
		true
	} count ([_group] call ACF_getGroupVehicles);

	{
		_result = _result + (_x call BIS_fnc_threat) + 0.3;
		true
	} count _groupUnits;

	_result = _result * 2 * (skill leader _group);

	SVAR(_group, "resetStr", false);
	SVAR(_group, "str", _result);
	_result
};


//UNUSED
ACF_ai_groupsStrength = {
	params ["_groups"];
	private _totalStr = 0;
	{
		private _vehicleStr = 0; // default str of vehicle
		{
			private _i = AC_vehicleStr findIf {typeOf _x};
			if (_i > -1) then {
				_vehicleStr = _vehicleStr + (AC_vehicleStr#_i);
			} else {
				_vehicleStr = _vehicleStr + 5;
			};
		} forEach ([_group] call ACF_getGroupVehicles);
		_totalStr = _totalStr + ({alive _x} count units _group) + _vehicleStr;
	} forEach _groups;

	_totalStr
};

/*
	PART 2: Planning defense and offensives

	This is brain of the defense decisions. Its main task is to assign
	enough defenders to the front line bases:

	Deciding algorithm:

	- How important is to defend the base: Bases should be defended
	according to their distance from FOB, manpower needed for defense, and avaliable troops

	Testcases:
		No problem: Nobody is attacking, all troops should be divided equally
		One base attack:
		Two-way attack:
		Losing side: Too many enemies attacking your front line
		Lost cause: Too many enemies everywhere, troops should make a last stand (and wait for reinforcements)

	Strategic calculations:

	1. Check if any units can be unassigned from defense
	2. Assign units for defense
	3. Check if offensive can be created: Check all enemy border bases

	---
	- counterattack as special defense function?
	- fn - ACF_findBorderBases; (all, friendly, enemy)
*/

ACF_ai_assignDefenders = {
	params ["_battalion"];
	private _side = GVAR(_battalion,"side");
	//Get a list of our frontier bases

	private _bases = [_side] call ACF_borderBases;
	_bases = _bases select {GVAR(_x,"side") == _side};
	private _hqGroup = GVARS(_battalion,"hqElement",grpNull);
	private _availableGroups = [];
	_availableGroups = AC_operationGroups select {
		side _x == _side
		&& {GVARS(_x,"canGetOrders",true)}
		&& {_x != _hqGroup}
		&& {GVAR(_x,"type") != TYPE_ARTILLERY}
	};

	private _basesPriority = [];
	{
		_basesPriority pushBack [_x, GVAR(_x,"def_costDet")];
		true
	} count _bases;

	//Concede badly outnumbered bases
	private _basesConceded = [];
	private _conceded = [];
	{
		_x params ["_base","_threatStr"];
		if ( (_threatStr > GVAR(_base,"def_currentStr") * RATIO_CONCEDE )
			&& {count _bases - count _basesConceded > 1}
		) then {
			_basesConceded pushBack [_base,_threatStr];
			_conceded pushback _base;

		};
		true
	} count _basesPriority;

	private _defendedBases = AC_bases select {
		GVAR(_x,"side") == _side
		&& {GVARS(_x,"defended",false)}
	};
	_defendedBases append _conceded;

	_basesPriority = _basesPriority - _basesConceded;

	// Free all assigned defenders from bases
	{
		private _base = _x;
		SVAR(_base,"defended", false);
		{
			SVAR(_x,"canGetOrders",true);
			_availableGroups pushBackUnique _x;
			true
		} count (GVARS(_base,"defenders",[]));
		true
	} count _defendedBases;

	if (count _availableGroups == 0) exitWith {};

	// Sort bases according to their defensive priority
	_basesPriority sort false;

	private ["_defenders", "_pos","_wp"];
	// Assign nearest groups as long there are defenders needed or no more units avaliable
	{
		_x params ["_base","_score"];
		_defenders = [];
		_pos = getPosWorld _base;

		// Sort groups by distance
		_availableGroups = [_availableGroups,[],{_pos distance2D leader _x},"ASCEND"] call BIS_fnc_sortBy;
		{
			if (_score < 0) exitwith {};
			if (_pos distance2D leader _x < REINFORCEMENTS_RANGE) then {
				// Make sure that only units comming to real defense are actually assigned
				_wp = [_x, _base, B_COMBAT, false,9] call ACF_ai_moveToBase;
				//_wp setWaypointType "GUARD";
				_score = _score - ([_x] call ACF_ai_groupStrength);
				_availableGroups = _availableGroups - [_x];
				_defenders pushBackUnique _x;
			};
			true
		} count _availableGroups;
		if (count _defenders > 0) then {
			SVAR(_base,"defended", true);
			SVAR(_base,"defenders", _defenders);
		};
		if (count _availableGroups == 0) exitWith {};
		true
	} count _basesPriority;
};

ACF_assignIdleGroups = {
	params ["_battalion"];
	private _side = GVAR(_battalion,"side");
	private _enemySide = [_side] call ACF_enemySide;
	private _hqGroup = GVARS(_battalion,"hqElement",grpNull);
	private _groups = [];
	_groups = AC_operationGroups select {
		side _x == _side
		&& {GVARS(_x,"canGetOrders",true)}
		&& {_x != _hqGroup}
		&& {GVAR(_x,"type") != TYPE_ARTILLERY}
	};

	if (count _groups < 1) exitWith {};

	private _frontlineBases = ([_side] call ACF_borderBases) select {GVAR(_x,"side") == _side};
	private _idleGroups = [];

	private _ongoingAttacks = GVARS(_battalion,"attacks",[]);

	if (count _ongoingAttacks > 0) then {
			{
				private _groupPos = getPosWorld leader _x;
				_ongoingAttacks = [_ongoingAttacks,[],{_x distance _groupPos},"ASCEND"] call BIS_fnc_sortBy;
				private _base = _ongoingAttacks#0;
				if (!isNil "_base" && {!isNull _base}) then {
					[_x, _base, B_TRANSPORT, true, 7] call ACF_ai_moveToStaging;
					_idleGroups pushback _x;
				};
			} forEach _groups;
	};

	_groups = _groups - _idleGroups;

	if (count _frontlineBases == 0) exitWith {};
	{
		private _groupPos = getPosWorld leader _x;
		_frontlineBases = [_frontlineBases,[],{_x distance _groupPos},"ASCEND"] call BIS_fnc_sortBy;
		private _base = _frontlineBases#0;
		if (!isNil "_base" && {!isNull _base}) then {
			[_x, _base, B_TRANSPORT, true, 7] call ACF_ai_moveToBase;
		};
	} forEach _groups;
};

// This is function that takes care of attack procedures: How is the attack synchronized, what are the formations, etc.
ACF_ai_createOffensive = {
	params ["_base","_groups","_side"];
	[_base,_side,_groups] spawn ACF_ai_offensiveAgent;
	//systemChat str ["Attacking:", GVAR(_base,"callsign")];
};

ACF_ai_battleEnded = {
	params ["_base","_winner"];
	SVAR(_base,"defended",true);

	private _aiSides = (AC_battalions select {GVARS(_x,"#commander","") == ""}) apply {GVAR(_x,"side")};
	{
		if (side _x in _aiSides) then {
			[_x, [_x] call ACF_rtbPos, B_TRANSPORT, true, 200] call ACF_ai_move;
		};
	} forEach (GVAR(_base,"defenders"));
	SVAR(_base,"defenders", []);
	[_base] call ACF_ai_calculateDefensesAndThreat;
};