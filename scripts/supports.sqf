#include "\AC\defines\commonDefines.inc"

// Handling fire support

/*
	Struktura supportu:
	Support má vlastní modul, který zaregistruješ k battalionu
	Support má vlastní strukturu v rámci battalionu

	Struktura:
		Side - ke kterýmu je to battalionu
		Type
		Ammo type
		nRounds
		Cost
		Timeout
*/

// Register supports in modules.
ACF_registerSupports = {
	params ["_battalion","_type"];
	private _side = GVAR(_battalion,"side");

	private _data = [];
	private _supports = [];
	if (_type != "Custom") then {
		_supports = getArray (configfile >> "AC" >> "Battalions" >> _type >> "Supports");

		// Data structure
		{
			private _cfg = configfile >> "AC" >> "Supports" >> _x;
			_data pushBack [
				getText 	(_cfg >> "name"), 			// 0
				getText 	(_cfg >> "tooltip"),		// 1
				getNumber 	(_cfg >> "cost"),			// 2
				getNumber 	(_cfg >> "timeout") + time,	// 3
				getNumber 	(_cfg >> "timeout"),		// 4
				getNumber 	(_cfg >> "type"),			// 5
				getText 	(_cfg >> "ammo"),			// 6
				getNumber 	(_cfg >> "nRounds"),		// 7
				getNumber 	(_cfg >> "radius"),			// 8
				{true}									// 9
			];
		} forEach _supports;
	};

	{
		if ([GVAR(_x,"side")] call AC_fnc_numberToSide == _side) then {
			private _condition = GVAR(_x,"condition");
			if(_condition == "") then {_condition = "true"};
			_data pushBack [
				GVAR(_x,"name"), 			// 0
				GVAR(_x,"tooltip"), 		// 1
				GVAR(_x,"cost"),			// 2
				GVAR(_x,"timeout") + time,	// 3
				GVAR(_x,"timeout"),			// 4
				SUPPORT_ARTY,				// 5
				GVAR(_x,"ammo"),			// 6
				GVAR(_x,"nRounds"),			// 7
				GVAR(_x,"radius"),			// 8
				compileFinal _condition		// 9
			];
		};
	} forEach (entities "AC_ModuleArtillerySupport");

	{
		if ([GVAR(_x,"side")] call AC_fnc_numberToSide == _side) then {
			private _condition = GVAR(_x,"condition");
			if(_condition == "") then {_condition = "true"};
			_data pushBack [
				GVAR(_x,"name"), 			// 0
				GVAR(_x,"tooltip"), 		// 1
				GVAR(_x,"cost"),			// 2
				GVAR(_x,"timeout") + time,	// 3
				GVAR(_x,"timeout"),			// 4
				SUPPORT_CAS,				// 5
				GVAR(_x,"Aircraft"),		// 6
				GVAR(_x,"SupportMode"),		// 7
				GVAR(_x,"Height"),			// 8
				compileFinal _condition	// 9
			];
		};
	} forEach (entities "AC_ModuleCAS");

	SVAR(_battalion,"ec_supportsList",_data);
};

#define SUPPORTS_ETA 20
// Server function
ACF_callSupport = {
	params ["_pos","_battalion","_index"];
	private _data = (GVAR(_battalion,"ec_supportsList")#_index);
	_data params ["_name","_tooltip","_cost","_timeout","_defaultTimeout","_type","_support","_nRounds","_radius","_condition"];

	// Remove commander's money, reset timeout
	private _side = GVAR(_battalion,"side");
	private _points = GVAR(_battalion,"points");
	if (_points < _cost) exitWith {};
	SVARG(_battalion,"points", _points - _cost);

	// Create fire support marker:
	if (_type == SUPPORT_CAS) then {_radius = 110};
	[_pos,_radius, _nRounds * 5 + SUPPORTS_ETA,_index] remoteExec ["ACF_drawSupportArea",_side];

	// Timeout before firing
	sleep SUPPORTS_ETA;

	if (_type == SUPPORT_ARTY) then {
		[_pos, _support, _radius, _nRounds, 4, {}, 0, 250, 150, ["shell1","shell2"]] spawn BIS_fnc_fireSupportVirtual;
	} else {
		[[_pos, 25] call ACF_randomPos, _support, _side, _battalion, _nRounds, _radius] spawn ACF_CAS;
	};
};

// Show or hide buy button
ACF_ui_buttonSupportsPressed = {
	// Find out position of the button in the table
	private _battalion = AC_playerBattalion;
	private _index = (ctCurSel UGVAR("ac_supportsList"));

	setMousePosition [0.5,0.5];
	AC_supportMode = true;

	// Wait for clicking or closing map
	waitUntil {AC_supportsPlaced#0 != 0 || {!visibleMap}};
	if (!visibleMap) exitWith{}; // do not fire if map closed!
	((UGVAR("ac_supportsList") ctRowControls _index)#5) ctrlEnable false;
	[AC_supportsPlaced, _battalion, _index] remoteExec ["ACF_callSupport",2];

	AC_supportMode = false;
	AC_supportsPlaced = [0,0,0];

	// Wait for UI update
	[] spawn {
		private _points = GVAR(AC_playerBattalion,"points");
		waitUntil {GVAR(AC_playerBattalion,"points") != _points};
		[] call ACF_ui_updateCommandUI;
	};
};

ACF_getSupportRadius = {
	private _index = ctCurSel UGVAR("ac_supportsList");
	private _data = GVAR(AC_playerBattalion,"ec_supportsList");
	private _radius = _data#_index#8;
	if (_data#_index#5 == SUPPORT_CAS) then {_radius = 110};
	_radius
};

ACF_drawSupportArea = {
	params ["_pos","_radius","_timeout","_index"];

	private _data = GVAR(AC_playerBattalion,"ec_supportsList");
	private _typeData = _data#_index;
	private _name = _typeData#0;

	// Restart timeout
	_typeData set [3,time + (_typeData#4)];
	SVAR(AC_playerBattalion,"ec_supportsList",_data);
	// Switch timeout to default timeout

	private _mrk = createMarkerLocal [str _pos,_pos];
	_mrk setMarkerShapeLocal "ELLIPSE";
	_mrk setMarkerSizeLocal [_radius,_radius];
	_mrk setMarkerAlphaLocal 0.5;

	private _mrk2 = createMarkerLocal [str (_pos + [0]),_pos];
	_mrk2 setMarkerShapeLocal "ICON";
	_mrk2 setMarkerTypeLocal "hd_destroy";
	_mrk2 setMarkerTextLocal _name;

	sleep (_timeout + 5);
	deleteMarkerLocal _mrk;
	deleteMarkerLocal _mrk2;
};

// HEAVILY modified BIS_fnc_moduleCAS from Karel Moricky
ACF_cas = {
	params ["_pos","_aircraft","_side","_caller","_weaponTypesID",["_height",800]];

	private _dir = _caller getDir _pos;
	//fly from nearby base or battalion location
	private _basesToSpawn = [_side] call ACF_findSpawnBases;
	_basesToSpawn = [_basesToSpawn,[],{_x distance _pos},"ASCEND"] call BIS_fnc_sortBy;
	if (count _basesToSpawn > 0) then {
		{
			if ( _x distance _pos > 850) exitwith { _caller = _x};
		} foreach _basesToSpawn;
	};

	_pos set [2,(_pos select 2) + getterrainheightasl _pos];

	//--- Detect gun
	_weaponTypes = switch _weaponTypesID do {
		case 0: {["machinegun","cannon","vehicleweapon"]};
		case 1: {["missilelauncher"]};
		case 2: {["machinegun","missilelauncher","cannon","vehicleweapon"]};
		case 3: {["bomblauncher"]};
		default {[]};
	};
	_weapons = [];
	{
		if (tolower ((_x call bis_fnc_itemType) select 1) in _weaponTypes) then {
			_modes = getarray (configfile >> "cfgweapons" >> _x >> "modes");
			if (count _modes > 0) then {
				_mode = _modes select 0;
				if (_mode == "this") then {_mode = _x;};
				_weapons set [count _weapons,[_x,_mode]];
			};
		};
	} foreach (_aircraft call bis_fnc_weaponsEntityType);


	//--- Create plane
	private _planePos = [getPosASL _caller,-800,_dir] call bis_fnc_relpos;
	//private _planePos = getPosASL _caller;
	private _dis = _caller distance _pos;
	private _alt = _height + ((_planePos#2 + getTerrainHeightASL _pos)/2);
	private _speed = 110;
	_planePos set [2,_alt];
	_dir = _caller getDir _pos;
	private _planeArray = [_planePos,_dir,_aircraft,_side] call bis_fnc_spawnVehicle;
	_plane = _planeArray#0;
	_pos set [2,0]; //This makes the bombs hit... for RHS anyway
	_plane move _pos;
	_plane limitSpeed _speed;
	_plane flyInHeightASL [_height, 20, (_height * 2)];
	_plane flyInHeight _height;
	_plane disableai "target";
	_plane disableai "autotarget";
	_plane setcombatmode "blue";

	//set starting velocity
	private _vel = velocity _plane;
	private _distance = 800;
	private _angle = 2.55;
	if (_weaponTypesID == 1) then {_angle = 2.45;};
	if (_weaponTypesID == 3) then {_distance = 450; _angle = 0.9;};
	_plane setVelocity [
		(_vel select 0) + (sin _dir * _speed),
		(_vel select 1) + (cos _dir * _speed),
		(_vel select 2)
	];

	_vectorDir = [_planePos,_pos] call bis_fnc_vectorFromXtoY;
	_plane setvectordir _vectorDir;
	[_plane,0,0] call bis_fnc_setpitchbank;

	//--- Approach
	_fire = [] spawn {waituntil {false}};
	_fireNull = true;
	_time = time;
	_pilot = driver _plane;
	waituntil {

		//--- Fire!
		if ( (getposasl _plane) distance _pos < _distance && _fireNull) then {
			_pilot setBehaviour "COMBAT";
			_plane flyInHeight 35;
			_planePos = getPosASL _plane;
			_pitchBank = vehicle _pilot call BIS_fnc_getPitchBank;
			_newPitchBank = [-90 + atan ( (_distance/_angle) / _planePos#2),0] vectorAdd _pitchBank;
			[_plane,_newPitchBank#0 / 2,_newPitchBank#1 / 2] call bis_fnc_setpitchbank;
			//--- Create laser target
			private _targetType = if (_side getfriend west > 0.6) then {"LaserTargetW"} else {"LaserTargetE"};
			_target = ((_pos nearEntities [_targetType,50])) param [0,objnull];
			if (isnull _target) then {
				_target = createvehicle [_targetType,_pos,[],0,"none"];
			};
			_plane reveal lasertarget _target;
			_plane dowatch lasertarget _target;
			_plane dotarget lasertarget _target;
			_fireNull = false;
			terminate _fire;
			_fire = [_plane,_weapons,_target,_weaponTypesID] spawn {
				_plane = _this select 0;
				_planeDriver = driver _plane;
				_weapons = _this select 1;
				_target = _this select 2;
				_weaponTypesID = _this select 3;
				_time = time + 3.2;
				waituntil {
					{
						private _fired = _planeDriver fireattarget [_target,(_x select 0)];
						if (!_fired) then {
							_plane selectweapon (_x select 0);
							_planeDriver forceweaponfire _x;
						};
					} foreach _weapons;
					sleep 0.1;
					time > _time || _weaponTypesID == 3 || isnull _plane //--- Shoot only for specific period or only one bomb
				};
				if (_weaponTypesID == 3) then {sleep 0.1;} else	{sleep 0.8};
			};
		};
		sleep 0.1;
		scriptdone _fire || isnull _plane
	};
	_plane limitSpeed 1000;
	_plane move ([_pos,-2000,_dir] call bis_fnc_relpos);
	_pilot setBehaviour "STEALTH";

	sleep 35;
	// Delete plane
	if (alive _plane) then {
		{deletevehicle _x} foreach (crew _plane);

		private _planegroup = group _plane;
	    _planegroup deleteGroupWhenEmpty true;
		if !(local _planegroup) then {
		    [_planegroup, true] remoteExec ["deleteGroupWhenEmpty", groupOwner _planegroup];
		};

		deletevehicle _plane;
	};
};